from flask import Flask
from flask_restful import Resource, Api
import markdown
import os
from flask_apispec.extension import FlaskApiSpec
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
# from apispec_webframeworks.flask import FlaskPlugin

# create an instance of Flask
app = Flask(__name__)
api = Api(app)


# swagger documentation 
app.config.update({
    'APISPEC_SPEC': APISpec(
        title='REST API DOC',
        version='v0.0',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0.0'
    ),
    'APISPEC_SWAGGER_URL': '/swagger_doc/',  
    'APISPEC_SWAGGER_UI_URL': '/swagger/'  
})


# documentation(docs) configuration
docs = FlaskApiSpec(app)


from resources import(
		#Login,
		Users		
	)



# api.add_resource(Login, "/login")

api.add_resource(Users, "/users")
docs.register(Users)


# route index page that used as main page
# @app.route('/hello')
# def index():
    
#     # open the README
#     with open(os.path.dirname(app.root_path) + '/README.md', 'r') as open_markdown:

#         # read the content from README file        
#         file_content = open_markdown.read()

#         # return README on html
#         return markdown.markdown(file_content)