from dataclasses import fields
from flask import request, jsonify
from flask_restful import Resource
from flask_apispec.views import MethodResource
from flask_apispec import doc, use_kwargs
from marshmallow import Schema, fields


# defining Schema object to get data from client
class UserSchema(Schema):
	username = fields.Str(required=True)
	password = fields.Str(required=True)




# class Login(MethodResource, Resource):
# 	def get(self):
# 		return {"message": "get login"}
		


class Users(MethodResource, Resource):

	@doc(description='get user', tags=["USERS"])
	def get(self):
		return {"message": "get users"}

	@doc(description="add user", tags=["USERS"])
	@use_kwargs(UserSchema, location=('json'))
	def post(self, **kwargs):
		try:
			request_data = request.json
			if(request_data.get("username") and request_data.get("username")):
				return jsonify({"welcome": request_data.get("username")})
		except Exception as e:
			return jsonify({"error message": str(e)})
